import random

class Venta:
    def __init__(self, nro, nombre, semana, familia, monto):
        self.nro = nro
        self.nombre = nombre
        self.semana = semana
        self.familia = familia
        self.monto = monto


def cargar_ventas(n):
    v = [None] * n
    for i in range(n):
        v[i] = cargar_factura_random()
    return v


def cargar_factura():
    nro = int(input("Ingrese número de factura: "))
    nombre = input("Ingrese nombre: ")
    semana = int(input("Ingrese semana: "))
    familia = int(input("Familia de producto: "))
    monto = float(input("Ingrese monto: "))
    venta = Venta(nro, nombre, semana, familia, monto)
    return venta


def cargar_factura_random():
    nro = random.randint(1, 10000)
    nombre = chr(random.randint(65, 90))
    semana = random.randint(1, 26)
    familia = random.randint(0, 9)
    monto = random.randint(100, 10000) / 10
    venta = Venta(nro, nombre, semana, familia, monto)
    return venta


def write(vta):
    print("Factura - Número:", vta.nro,
          "Nombre:", vta.nombre,
          "Semana:", vta.semana,
          "Familia:", vta.familia,
          "Monto:", vta.monto)


def mostrar_ventas(v):
    for vta in v:
        write(vta)


def filtrar_familia(v, familia):
    aux = []
    for vta in v:
        if vta.familia == familia:
            aux.append(vta)
    return aux


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].semana > v[j].semana:
                v[i], v[j] = v[j], v[i]


def venta_mayor_importe(v):
    mayor = v[0]
    for i in range(1, len(v)):
        if v[i].monto > mayor.monto:
            mayor = v[i]
    return mayor


def buscar_venta(v, nro):
    resultado = None
    for vta in v:
        if vta.nro == nro:
            resultado = vta
            break
    return resultado


def principal():
    op = 0
    v = []
    while op != 6:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de ventas: "))
            v = cargar_ventas(n)
        elif op == 2:
            mostrar_ventas(v)
        elif op == 3:
            familia = int(input("Ingrese familia: "))
            ventas = filtrar_familia(v, familia)
            ordenar(ventas)
            print("Ventas de la familia", familia, ":")
            mostrar_ventas(ventas)
        elif op == 4:
            mayor = venta_mayor_importe(v)
            print("La venta con mayor importe es:")
            write(mayor)
        elif op == 5:
            nro = int(input("Ingrese número de venta: "))
            res = buscar_venta(v, nro)
            if res == None:
                print("No se encontró")
            else:
                print("Datos de la venta:")
                write(res)

def menu():
    print("1 _ Cargar Ventas")
    print("2 _ Mostrar Ventas")
    print("3 _ Listar por familia")
    print("4 _ Buscar venta de mayor importe")
    print("5 _ Buscar por número de factura")
    print("6 _ Salir")
    op = int(input("Ingrese opción: "))
    return op


if __name__ == '__main__':
    principal()
