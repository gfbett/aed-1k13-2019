LETRAS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÑabcdefghijklmnopqrstuvwxyzñ"


def es_vocal(car):
    return car in ("a", "e", "i", "o", "u")


def es_letra(car):
    return car in LETRAS


#texto = input("Ingrese texto")
texto = "hola     mundo"
texto += "."


# inicializacion
clet = cpal = 0
band_s = band_si = band_vocal = False
palabras_si = palabras_vocal_impar = 0


for car in texto:
    if es_letra(car):
        # dentro de palabra
        clet += 1
        if car == "s" and clet == 1:
            band_s = True
        else:
            if band_s and car == "i":
                band_si = True
            band_s = False

        if es_vocal(car):
            band_vocal = True
        else:
            band_vocal = False
    else:
        if clet > 0:
            # fin de palabra
            cpal += 1
            if band_si:
                palabras_si += 1
            if band_vocal and clet % 2 != 0:
                palabras_vocal_impar += 1

        clet = 0

# fin de texto

print("Texto:", texto)
print("Palabras que empiezan con si", palabras_si)

