hora_salida = int(input("Ingrese hora de partida: "))
minutos_salida = int(input("Ingrese minutos de partida: "))
hora_llegada = int(input("Ingrese hora de llegada: "))
minutos_llegada = int(input("Ingrese minutos de llegada: "))

salida_min = hora_salida * 60 + minutos_salida
llegada_min = hora_llegada * 60 + minutos_llegada

duracion = llegada_min - salida_min
print("El vuelo tiene una duración de", duracion, "minutos")

hotel = llegada_min + 45
hora, minutos = divmod(hotel, 60)
print("Se llega al hotel a las", hora, ":", minutos)
