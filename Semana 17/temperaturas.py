import random


def cargar_mediciones(n):
    temp = [0] * n
    region = [0] * n
    dia = [0] * n

    for i in range(n):
        temp[i] = random.randint(-6, 40)
        region[i] = random.randint(1, 20)
        dia[i] = random.randint(1, 31)

    return temp, region, dia


def calcular_promedio(temp):
    suma = 0
    n = len(temp)
    for t in temp:
        suma += t
    return suma / n


def ordenar(temp, region, dia):
    n = len(temp)
    for i in range(n-1):
        for j in range(i+1, n):
            if dia[i] > dia[j]:
                dia[i], dia[j] = dia[j], dia[i]
                temp[i], temp[j] = temp[j], temp[i]
                region[i], region[j] = region[j], region[i]


def listar_temperaturas(temp, region, dia, x):
    print("Temperaturas de la región", x)
    for i in range(len(region)):
        if region[i] == x:
            print("Dia: ", dia[i], "Temperatura: ", temp[i])


def contar_por_region(regiones):
    cont = [0] * 20

    for region in regiones:
        cont[region-1] += 1

    return cont


def mayor_region(region, temp, r, x):
    for i in range(len(region)):
        if region[i] == r and temp[i] > x:
            return True
    return False


def principal():
    n = int(input("Ingrese cantidad de mediciones: "))
    while n <= 0:
        n = int(input("Error. Ingrese cantidad de mediciones: "))

    temp, region, dia = cargar_mediciones(n)
    mostrar_datos(dia, region, temp)

    promedio = calcular_promedio(temp)
    print("El promedio de temperatura es:", promedio)

    ordenar(temp, region, dia)
    x = int(input("Ingrese región: "))
    listar_temperaturas(temp, region, dia, x)

    r = int(input("Ingrese región: "))
    x = int(input("Ingrese temperatura "))
    hay_mayor = mayor_region(region, temp, r, x)
    if hay_mayor:
        print("Hay temperatura que supera a x")
    else:
        print("NO Hay temperatura que supera a x")

    cantidad_region = contar_por_region(region)
    for i in range(len(cantidad_region)):
        if cantidad_region[i] != 0:
            print("Region:", i+1, "Cantidad:", cantidad_region[i])


def mostrar_datos(dia, region, temp):
    print(temp)
    print(region)
    print(dia)


if __name__ == '__main__':
    principal()
