def es_letra(c):
    return c != " " and c != "."


def es_vocal(letra):
    return letra in ("a", "e", "i", "o", "u")


texto = input("Ingrese texto: ")
#texto = "Ahora que vengan y nos ganen si pueden."

# inicialización
clet = cpal = cont_vocales = 0
palabras_vocal_4 = palabras_vp_na = palabras_ga = 0
es_consonante_2da = empieza_vp = termina_na = False
band_g = band_ga = False
menor_cons_2da = None

for car in texto:
    if es_letra(car):
        # Dentro de palabra
        clet += 1
        # punto 1
        if es_vocal(car):
            cont_vocales += 1
        # punto 2
        if clet == 2 and not es_vocal(car):
            es_consonante_2da = True
        # punto 3
        if clet == 1 and (car == 'v' or car == 'p'):
            empieza_vp = True
        if car == 'a' or car == 'n':
            termina_na = True
        else:
            termina_na = False
        # punto 4
        if car == 'g':
            band_g = True
        else:
            if car == 'a' and band_g:
                band_ga = True
            band_g = False

    else:
        if clet > 0:
            # Fin de palabra
            cpal += 1
            # punto 1
            if clet > 4 and cont_vocales >= 3:
                palabras_vocal_4 += 1
            # punto 2
            if es_consonante_2da:
                if menor_cons_2da == None or clet < menor_cons_2da:
                    menor_cons_2da = clet
            # punto 3
            if empieza_vp and termina_na:
                palabras_vp_na += 1
            # punto 4
            if band_ga:
                palabras_ga += 1

        clet = cont_vocales = 0
        es_consonante_2da = empieza_vp = termina_na = False
        band_g = band_ga = False


# Fin de texto
print("Hay", cpal, "palabras")
print("Hay", palabras_vocal_4, "palabras con mas de 4 letras y 3 vocales")
print("La palabra mas corta con consonante en la segunda tiene", menor_cons_2da, "letras")
print("Palabras que empiezan con vp y terminan con na", palabras_vp_na)
if palabras_ga > 0:
    porcentaje = palabras_ga * 100 / cpal
    print("El porcentaje de palabras con ga es: ", porcentaje, "%", sep="")
else:
    print("No hay palabras con ga")
