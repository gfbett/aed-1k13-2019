# Entrada
codigo1 = input("Ingrese el código del 1er curso: ")
varones1 = int(input("Ingrese la cantidad de varones: "))
mujeres1 = int(input("Ingrese la cantidad de mujeres: "))

codigo2 = input("Ingrese el código del 2do curso: ")
varones2 = int(input("Ingrese la cantidad de varones: "))
mujeres2 = int(input("Ingrese la cantidad de mujeres: "))

codigo3 = input("Ingrese el código del 3er curso: ")
varones3 = int(input("Ingrese la cantidad de varones: "))
mujeres3 = int(input("Ingrese la cantidad de mujeres: "))

cupo = int(input("Ingrese el cupo máximo: "))

#Procesos
total_curso1 = varones1 + mujeres1
total_curso2 = varones2 + mujeres2
total_curso3 = varones3 + mujeres3

if total_curso1 < total_curso2 and total_curso1 < total_curso3:
    menor_codigo = codigo1
elif total_curso2 < total_curso3:
    menor_codigo = codigo2
else:
    menor_codigo = codigo3

porc_mujeres1 = mujeres1 * 100 / total_curso1
porc_mujeres2 = mujeres2 * 100 / total_curso2
porc_mujeres3 = mujeres3 * 100 / total_curso3

porc_varones1 = varones1 * 100 / total_curso1
porc_varones2 = varones2 * 100 / total_curso2
porc_varones3 = varones3 * 100 / total_curso3

promedio = (total_curso1 + total_curso2 + total_curso3) / 3

#Salida
print("El curso con menor cantidad de alumnos es:", menor_codigo)

print("El porcentaje de mujeres del curso", codigo1, "es", porc_mujeres1, "%")
print("El porcentaje de mujeres del curso", codigo2, "es", porc_mujeres2, "%")
print("El porcentaje de mujeres del curso", codigo3, "es", porc_mujeres3, "%")

print("El porcentaje de varones del curso", codigo1, "es", porc_varones1, "%")
print("El porcentaje de varones del curso", codigo2, "es", porc_varones2, "%")
print("El porcentaje de varones del curso", codigo3, "es", porc_varones3, "%")

print("La cantidad promedio de alumnos por curso es: ", promedio)

if total_curso1 > cupo or total_curso2 > cupo or total_curso3 > cupo:
    print("Debe abrir una nueva comisión.")

