import random
VOCALES = "aeiou"
CONSONANTES = "bcdfgjklmnpqrstvwxyz"


def string_random(n):
    res = ""
    for i in range(n):
        res += random.choice(CONSONANTES)
        res += random.choice(VOCALES)

    return res

class Alumno:
    def __init__(self, legajo, nombre, nota ):
        self.nombre = nombre
        self.nota = nota
        self.legajo = legajo


def write(alumno):
    print( "Legajo:", alumno.legajo ,
           "Nombre: ",alumno.nombre,
           "Nota:", alumno.nota,
         )


def cargar_datos(n):
    v = [None] * n
    for i in range(n):
        v[i] = cargar_alumno_random()
    return v


def cargar_alumno():
    legajo = int(input("Ingrese legajo: "))
    nombre = input("Ingrese nombre: ")
    nota = int(input("Ingrese nota: "))
    alumno = Alumno(legajo, nombre, nota)
    return alumno


def cargar_alumno_random():
    legajo = random.randint(30000, 60000)
    nombre = string_random(3)
    nota = random.randint(1, 10)
    alumno = Alumno(legajo, nombre, nota)
    return alumno


def mostrar_alumnos(v):
    # for i in range(len(v)):
    #     write(v[i])
    for alumno in v:
        write(alumno)


def calcular_promedio(v):
    n = len(v)
    suma = 0
    for i in range(n):
        suma += v[i].nota
    return suma / n


def ordenar(v):
    n = len(v)
    for i in range(n-1):
        for j in range(i+1,n):
            if v[i].nombre > v[j].nombre:
                v[i], v[j] = v[j], v[i]


def principal():
    n = int(input("Ingrese cantidad de alumnos: "))
    alumnos = cargar_datos(n)
    mostrar_alumnos(alumnos)
    promedio = calcular_promedio(alumnos)
    print("La nota promedio es: ", promedio)
    ordenar(alumnos)
    mostrar_alumnos(alumnos)


if __name__ == '__main__':
    principal()
