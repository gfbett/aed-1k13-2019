# Una fábrica de zapatillas realiza una encuesta para determinar cuál es el calzado
# que tiene mayor demanda en los jóvenes entre 10 y 25 años. La encuesta determinaba:
#
#   - Número que calza
#   - Color de preferencia (blanco, negro, azul)
#   - Edad
#   - Qué prefiere: cuero o tela
#
# Hacer un programa que permita cargar la información en 4 vectores  de  las 100
# encuestas realizadas y determinar las siguientes estadísticas:
#
#   - Cuál es el número de zapatillas promedio.
#   - Los jóvenes entre 10  y 18 años  que color prefieren mas.
#   - Cuál es la preferencia de los jóvenes entre 19 y 25 años, con respecto al
#     material de las zapatillas (cuero o tela).
#   - Si el numero de calzado va desde 35 a 40, generar un vector que contenga cual
#     es la demanda de cada número de calzado (un vector contador de 6 elementos) y
#     determinar cuál es el número de mayor y menor demanda.
import random
BLANCO = "Blanco"
NEGRO = "Negro"
AZUL = "Azul"


def cargar_datos(n):
    nros = [0] * n
    colores = [0] * n
    edades = [0] * n
    materiales = [0] * n

    for i in range(n):
        nros[i] = random.randint(20, 50)
        colores[i] = random.choice((BLANCO, NEGRO, AZUL))
        edades[i] = random.randint(10, 25)
        materiales[i] = random.randint(1, 2)

    return nros, colores, edades, materiales


def mostrar(nros, colores, edades, materiales):
    print(nros)
    print(colores)
    print(edades)
    print(materiales)


def calcular_nro_promedio(nros):
    suma = 0
    for nro in nros:
        suma += nro
    return suma / len(nros)


def calcular_color_preferido(edades, colores):
    cblanco = cazul = cnegro = 0
    n = len(edades)
    for i in range(n):
        if edades[i] >= 10 and edades[i] <= 18:
            if colores[i] == BLANCO:
                cblanco += 1
            elif colores[i] == NEGRO:
                cnegro += 1
            elif colores[i] == AZUL:
                cazul += 1

    if cblanco > cazul and cblanco > cnegro:
        mayor = BLANCO
    elif cazul > cnegro:
        mayor = AZUL
    else:
        mayor = NEGRO

    return mayor


def calcular_material_preferido(edades, materiales):
    cont = [0] * 2
    n = len(edades)
    for i in range(n):
        if edades[i] >= 19 and edades[i] <= 25:
            material = materiales[i]
            cont[material - 1] += 1

    if cont[0] > cont[1]:
        mayor = "Cuero"
    else:
        mayor = "Tela"

    return mayor


# def verificar_rango(nros):
#     maximo = nros[0]
#     minimo = nros[0]
#     for i in range(1, len(nros)):
#         if nros[i] > maximo:
#             maximo = nros[i]
#         if nros[i] < minimo:
#             minimo = nros[i]
#
#     return minimo == 35 and maximo == 40


def determinar_demanda(nros):
    cont = [0] * 6
    for nro in nros:
        if 35 <= nro <= 40:
            indice = nro - 35
            cont[indice] += 1
    return cont


def buscar_posicion_mayor(v):
    mayor = v[0]
    mayor_pos = 0
    for i in range(1, len(v)):
        if v[i] > mayor:
            mayor = v[i]
            mayor_pos = i
    return mayor_pos


def buscar_posicion_menor(v):
    menor = v[0]
    menor_pos = 0
    for i in range(1, len(v)):
        if v[i] < menor:
            menor = v[i]
            menor_pos = i
    return menor_pos


def principal():
    nros, colores, edades, materiales = cargar_datos(100)
    mostrar(nros, colores, edades, materiales)

    nro_promedio = calcular_nro_promedio(nros)
    print("El número promedio es:", nro_promedio)

    color_preferido = calcular_color_preferido(edades, colores)
    print("El color preferido es:", color_preferido)

    material_preferido = calcular_material_preferido(edades, materiales)
    print("El material preferido es:", material_preferido)

    demanda = determinar_demanda(nros)
    print(demanda)
    mayor = buscar_posicion_mayor(demanda)
    print("El nro con mas demanda es:", mayor + 35)
    menor = buscar_posicion_menor(demanda)
    print("El nro con menos demanda es:", menor + 35)
    
    
if __name__ == '__main__':
    principal()
