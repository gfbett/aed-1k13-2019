import random

# entre [0, 1)
a = random.random()
print(a)

# entre [10, 55]
b = random.randint(10, 55)
print(b)

# entre [10, 55)
c = random.randrange(10, 55)
print(c)

# Devuelve un elemento random de la secuencia
valores = "abcdefghijklmnñopqrstuvwxyz"
d = random.choice(valores)
print(d)
