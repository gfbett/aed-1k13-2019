import pickle
import invitados


def generar_archivo(n):
    """
    Generar un archivo de invitados
    :param n: Cantidad de invitados
    """
    archivo = open("invitados.bin", "wb")
    for i in range(n):
        invitado = invitados.cargar_invitado_random()
        pickle.dump(invitado, archivo)
    archivo.close()
    print("Se generaron", n, "invitados exitosamente.")


def principal():
    n = int(input("Ingrese cantidad de invitados: "))
    generar_archivo(n)


if __name__ == '__main__':
    principal()
