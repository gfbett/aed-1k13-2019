import pickle
import os
import invitados

ARCHIVO = "invitados.bin"
ONGS = ("Missing Children", "Aldeas", "Caritas", "Fundaleu", "PUPI", "Cimientos", "Médicos Sin Fronteras", "Uniendo Caminos", "Vida Silvestre", "Adoptare")


def menu():
    print("1_ Listar invitados")
    print("2_ Contar por mesa")
    print("3_")
    print("4_ Generar archivo ong")
    print("5_ Total archivo ong")
    print("6_ Salir")
    return int(input("Ingrese opción: "))


def insertar_ordenado(v, invitado):
    n = len(v)
    izq = 0
    der = n - 1
    pos = n
    while izq <= der:
        c = (izq + der) // 2
        if v[c].nombre == invitado.nombre:
            pos = c
            break
        elif invitado.nombre > v[c].nombre:
            izq = c + 1
        else:
            der = c - 1

    if izq > der:
        pos = izq

    v[pos:pos] = [invitado]


def cargar_invitados():
    v = []
    archivo = open(ARCHIVO, "rb")
    size = os.path.getsize(ARCHIVO)
    while archivo.tell() < size:
        invitado = pickle.load(archivo)
        insertar_ordenado(v, invitado)

    archivo.close()
    return v


def mostrar_invitados(v):
    for invitado in v:
        print(invitados.write(invitado))


def contar_invitados(v):
    m = [[0] * 10 for i in range(13)]
    for invitado in v:
        mesa = invitado.mesa
        ong = invitado.ong
        m[mesa][ong] += 1
    return m


def mostrar_cantidades(cantidades):
    for i in range(len(cantidades)):
        for j in range(len(cantidades[i])):
            if cantidades[i][j] > 0:
                print("ONG:", ONGS[j], "Mesa:", i, "Invitados:", cantidades[i][j])


def generar_archivo_ong(v, ong):
    nombre = "donaciones" + str(ong) + ".bin"
    archivo = open(nombre, "wb")
    for invitado in v:
        if invitado.ong == ong:
            pickle.dump(invitado, archivo)
    archivo.close()


def totalizar_mesas_ong(v, ong):
    totales = [0] * 13
    nombre = "donaciones" + str(ong) + ".bin"
    if os.path.exists(nombre):
        archivo = open(nombre, "rb")
        size = os.path.getsize(nombre)
        while archivo.tell() < size:
            invitado = pickle.load(archivo)
            totales[invitado.mesa] += invitado.monto
        archivo.close()
        print(totales)
    else:
        print("No se generó el archivo para la ong solicitada.")


def buscar_mayor(v, ong):
    mayor = None
    for invitado in v:
        if invitado.ong == ong:
            if mayor is None or invitado.monto > mayor.monto:
                mayor = invitado
    return mayor


def principal():
    op = 0
    while op != 6:
        op = menu()
        if op == 1:
            v = cargar_invitados()
            mostrar_invitados(v)
        elif op == 2:
            cantidades = contar_invitados(v)
            mostrar_cantidades(cantidades)
        elif op == 3:
            ong = int(input("Ingrese código de ong: "))
            mayor = buscar_mayor(v, ong)
            if mayor is None:
                print("No hay invitados de la ong")
            else:
                print("Datos del mayor:")
                print(invitados.write(mayor))
        elif op == 4:
            ong = int(input("Ingrese código de ong: "))
            generar_archivo_ong(v, ong)
        elif op == 5:
            ong = int(input("Ingrese código de ong: "))
            totalizar_mesas_ong(v, ong)


if __name__ == '__main__':
    principal()
