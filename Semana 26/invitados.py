import random

NOMBRES = "Juan", "Laura", "Roma", "Leon", "Pepe", "Tomas", "Enrique", "Ana", "Aldo"


class Invitado:
    def __init__(self, nombre, ong, mesa, monto):
        self.nombre = nombre
        self.ong = ong
        self.mesa = mesa
        self.monto = monto


def write(invitado):
    return "Nombre:{:<10} ONG:{:<2} Mesa:{:<2} Monto:${}".format(
        invitado.nombre, invitado.ong, invitado.mesa, invitado.monto
    )


def cargar_invitado_random():
    nombre = random.choice(NOMBRES)
    ong = random.randint(0, 9)
    mesa = random.randint(0, 12)
    monto = round(random.uniform(1000, 10000), 2)
    return Invitado(nombre, ong, mesa, monto)


if __name__ == '__main__':
    invitado = cargar_invitado_random()
    print(write(invitado))
