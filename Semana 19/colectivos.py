import random


def cargar_pasajeros(n, m):
    pasajeros = [[0] * m for i in range(n)]

    for i in range(n):
        for j in range(m):
            pasajeros[i][j] = random.randint(1, 15)

    return pasajeros


def pasajeros_linea(pasajeros):
    totales = [0] * len(pasajeros)
    for linea in range(len(pasajeros)):
        suma = 0
        for parada in range(len(pasajeros[linea])):
            suma += pasajeros[linea][parada]
        totales[linea] = suma
    return totales


def calcular_promedio(pasajeros, parada):
    suma = 0
    n = len(pasajeros)
    for linea in range(n):
        suma += pasajeros[linea][parada]
    return suma / n


def buscar_menor_linea(pasajeros, linea):
    menor = pasajeros[linea][0]
    for parada in range(1, len(pasajeros[linea])):
        if pasajeros[linea][parada] < menor:
            menor = pasajeros[linea][parada]
    return menor


def totalizar(pasajeros):
    suma = 0
    for linea in range(len(pasajeros)):
        for parada in range(len(pasajeros[linea])):
            suma += pasajeros[linea][parada]

    return suma


def principal():
    n = int(input("Ingrese cantidad de líneas: "))
    m = int(input("Ingrese cantidad de paradas: "))

    pasajeros = cargar_pasajeros(n, m)
    print(pasajeros)
    totales = pasajeros_linea(pasajeros)
    for i in range(len(totales)):
        print("La linea", i, "tuvo", totales[i], "pasajeros")

    parada = int(input("Ingrese parada a procesar: "))
    promedio = calcular_promedio(pasajeros, parada)
    print("El promedio de pasajeros de la parada es:", promedio)

    linea = int(input("Ingrese linea a procesar: "))
    menor = buscar_menor_linea(pasajeros, linea)
    print("La menor cantidad de pasajeros fue:", menor)

    total_pasajeros = totalizar(pasajeros)
    recaudacion = total_pasajeros * 8.5
    print("El monto total es:", recaudacion)


if __name__ == '__main__':
    principal()
