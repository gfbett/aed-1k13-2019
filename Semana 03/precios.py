# Entrada
precio_lista = float(input("Ingrese el precio de lista: "))

# Proceso
precio_contado = precio_lista * 0.9
precio_tarjeta = precio_lista * 1.05
# equivalente a : precio_tarjeta = precio_lista + precio_lista/20

# Salida
print("El precio de contado es de $: ", precio_contado)
print("El precio con tarjeta es de $: ", precio_tarjeta)
