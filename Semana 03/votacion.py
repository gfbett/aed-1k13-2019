# Entrada
votos_favor = int(input("Ingrese la cantidad de votos a favor: "))
votos_contra = int(input("Ingrese la cantidad de votos en contra: "))

# Proceso
total = votos_contra + votos_favor
porcentaje_favor = votos_favor / total * 100
porcentaje_contra = votos_contra / total * 100

# Salida
print("El porcentaje de votos a favor es de", round(porcentaje_favor, 2), "%")
print("El porcentaje de votos en contra es de", round(porcentaje_contra, 2), "%")
