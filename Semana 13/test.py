import random
import test2


def sumar(a, b, c=0, d=0):
    return a + b + c + d


def sumar2(a, b, *args, inicial = 0):
    suma = inicial
    for valor in args:
        suma += valor
    return suma + a + b



print(sumar(1, 2))
print(sumar(1, 2, 3))
print(sumar(1, 2, 3, 4))
print(sumar(1, 2, d=5))
print(sumar2(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, inicial=40))

test2.hola()

