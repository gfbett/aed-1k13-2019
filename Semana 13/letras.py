LETRAS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÑabcdefghijklmnopqrstuvwxyzñ"


def es_vocal(car):
    return car in ("a", "e", "i", "o", "u")


def es_letra(car):
    return car in LETRAS


# texto = input("Ingrese texto")
texto = "hola     mundo muy algo ocaso con accion"
texto += "."


# inicializacion
clet = cpal = cvocales = menor = 0
band_s = band_si = band_vocal = False
band_c = band_cc = False
palabras_si = palabras_vocal_impar = palabras_una_vocal = palabras_misma_letra = 0
palabras_cc = total_letras = 0
primera_letra = ultima_letra = ""


for car in texto:
    if es_letra(car):
        # dentro de palabra
        clet += 1
        if car == "s" and clet == 1:
            band_s = True
        else:
            if band_s and car == "i":
                band_si = True
            band_s = False

        if car == "c" and not band_c:
            band_c = True
        else:
            if car == "c":
                band_cc = True
            band_c = False

        if es_vocal(car):
            cvocales += 1
            band_vocal = True
        else:
            band_vocal = False

        if clet == 1:
            primera_letra = car
        ultima_letra = car

    else:
        if clet > 0:
            # fin de palabra
            cpal += 1
            total_letras += clet
            if band_si:
                palabras_si += 1
            if band_cc:
                palabras_cc += 1
            if band_vocal and clet % 2 != 0:
                palabras_vocal_impar += 1
            if cvocales == 1:
                palabras_una_vocal += 1
            if primera_letra == ultima_letra:
                palabras_misma_letra += 1
            if cpal == 1 or clet < menor:
                menor = clet

        clet = cvocales = 0
        band_s = band_si = band_vocal = False

# fin de texto

print("Texto:", texto)
print("Palabras que empiezan con si:", palabras_si)
print("Palabras que terminan en vocal con longitud impar:", palabras_vocal_impar)
print("Palabras con una vocal:", palabras_una_vocal)
print("Cantidad de palabras que empiezan y terminan en la misma letra:", palabras_misma_letra)
print("Cantidad de palabras que contienen cc:", palabras_cc)

porcentaje = palabras_vocal_impar * 100 / cpal
print("Porcentaje de las palabras del punto b:", round(porcentaje, 2), "%")
print("Longitud de la palabra mas corta:", menor)

promedio = total_letras / cpal
print("Promedio de letras: ", promedio)
