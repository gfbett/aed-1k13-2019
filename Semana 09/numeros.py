n = int(input("Ingrese la cantidad de números: "))
while n <= 0:
    n = int(input("ERROR!! Ingrese un valor positivo: "))

es_primer_par = True
es_primer_impar = True

for i in range(n):
    num = int(input("Ingrese un número: "))
    while num <= 0:
        num = int(input("ERROR!! Ingrese un valor positivo: "))

    # verifico par
    if num % 2 == 0:
        if es_primer_par:
            mayor = num
            es_primer_par = False
        elif num > mayor:
            mayor = num
    else:
        if es_primer_impar:
            menor = num
            es_primer_impar = False
        elif num < menor:
            menor = num

if not es_primer_par:
    print("El mayor de los pares es:", mayor)
else:
    print("No se ingresaron números pares")

if not es_primer_impar:
    print("El menor de los impares es:", menor)
else:
    print("No se ingresaron números impares")
