import alumnos
import pickle
import os.path

NOMBRE_ARCHIVO = "alumno.dat"


def menu():
    print("1 _ Agregar alumno")
    print("2 _ Listar alumnos")
    print("3 _ Salir")
    return int(input("Ingrese opción: "))


def grabar(v):
    archivo = open(NOMBRE_ARCHIVO, "wb")
    for alumno in v:
        pickle.dump(alumno, archivo)
    archivo.close()


def cargar():
    v = []
    if os.path.exists(NOMBRE_ARCHIVO):
        size = os.path.getsize(NOMBRE_ARCHIVO)
        archivo = open(NOMBRE_ARCHIVO, "rb")
        while archivo.tell() < size:
            alumno = pickle.load(archivo)
            v.append(alumno)
        archivo.close()
    return v


def principal():
    op = 0
    v = cargar()
    while op != 3:
        op = menu()
        if op == 1:
            alumno = alumnos.cargar_alumno()
            v.append(alumno)
        elif op == 2:
            for a in v:
                alumnos.write(a)
        elif op == 3:
            grabar(v)



if __name__ == '__main__':
    principal()
