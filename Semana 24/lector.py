import alumnos
import pickle
import os.path

nombre_archivo = "datos.bin"

if os.path.exists(nombre_archivo):
    tam = os.path.getsize(nombre_archivo)
    archivo = open(nombre_archivo, "rb")
    while archivo.tell() < tam:
        alumno = pickle.load(archivo)
        alumnos.write(alumno)

    archivo.close()
else:
    print("No existe el archivo")
