import alumnos
import os.path

nombre_archivo = "datos.txt"

if os.path.exists(nombre_archivo):
    archivo = open(nombre_archivo, "rt")

    for linea in archivo:
        partes = linea.split(",")
        a = alumnos.Alumno(partes[0], partes[1].strip())
        alumnos.write(a)


    archivo.close()
else:
    print("No existe el archivo")
