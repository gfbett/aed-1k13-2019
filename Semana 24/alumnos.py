class Alumno:
    def __init__(self, legajo, nombre):
        self.legajo = legajo
        self.nombre = nombre


def write(alumno):
    print("Legajo:", alumno.legajo,
          "Nombre:", alumno.nombre)


def cargar_alumno():
    legajo = int(input("Ingrese legajo: "))
    nombre = input("Ingrese nombre: ")
    return Alumno(legajo, nombre)
