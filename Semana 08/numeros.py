sumatoria = 0
cantidad_pares = cantidad_impares = 0
hay_ceros = False
alternados = True
paridad = 0

num = int(input("Ingrese un número: "))
while num >= 0:
    if num >= 50 and num <= 100:
        sumatoria = sumatoria + num

    if num % 2 == 0:
        cantidad_pares += 1
        if paridad == 1:
            alternados = False
        paridad = 1

    else:
        cantidad_impares += 1
        if paridad == 2:
            alternados = False
        paridad = 2

    if num == 0:
        hay_ceros = True

    num = int(input("Ingrese un número: "))

print("La sumatoria de los números entre 50 y 100 es", sumatoria)
print("Hay", cantidad_pares, "números pares")
print("Hay", cantidad_impares, "números impares")
if hay_ceros:
    print("Se ingresaron ceros en la secuencia")
if alternados:
    print("Los números ingresados alternan pares e impares")
else:
    print("No hay alternancia de pares e impares")
