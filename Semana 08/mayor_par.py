cantidad = 0
primer_par_posicion_par = True
num = int(input("Ingrese un número: "))
while num != 0:
    cantidad += 1

    if num % 2 == 0 and cantidad % 2 == 0:
        if primer_par_posicion_par:
            mayor = num
            primer_par_posicion_par = False
        elif num > mayor:
            mayor = num

    num = int(input("Ingrese un número: "))

if primer_par_posicion_par:
    print("No hay números pares en posición par.")
else:
    print("El mayor par es:", mayor)
