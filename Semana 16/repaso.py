import random


def menu():
    print("1 _ Generar arreglo")
    print("2 _ Promedio de números entre a y b")
    print("3 _ Menor número impar")
    print("4 _ Múltiplos de x")
    print("5 _ Salir")
    op = int(input("Ingrese opción: "))
    return op


def generar_arreglo(n):
    v = [0] * n
    for i in range(n):
        v[i] = random.randint(1, 1000)
    return v


def calcular_promedio_rango(v, desde, hasta):
    suma = 0
    cantidad = 0
    for num in v:
        if desde <= num <= hasta:
            suma += num
            cantidad += 1

    if cantidad > 0:
        promedio = suma / cantidad
    else:
        promedio = 0
    return promedio


def buscar_menor_impar(v):
    menor = -1
    for num in v:
        if num % 2 != 0:
            if menor == -1 or num < menor:
                menor = num
    return menor


def buscar_multiplos(v, x):
    multiplos = []
    for num in v:
        if num % x == 0:
            multiplos.append(num)
    return multiplos

def main():
    v = []
    op = 0
    while op != 4:
        op = menu()

        if op == 1:
            n = int(input("Ingrese cantidad de elementos: "))
            v = generar_arreglo(n)
            print(v)

        elif op == 2:
            a = int(input("Ingrese a: "))
            b = int(input("Ingrese b: "))
            promedio = calcular_promedio_rango(v, a, b)
            print("El promedio es:", promedio)

        elif op == 3:
            menor_impar = buscar_menor_impar(v)
            if menor_impar != -1:
                print("El menor impar es:", menor_impar)
            else:
                print("No hay números impares")

        elif op == 4:
            num = int(input("Ingrese número: "))
            multiplos = buscar_multiplos(v, num)
            for i in range(len(multiplos)):
                print(multiplos[i], end=", ")

        elif op == 5:
            print("Hasta luego!")


if __name__ == '__main__':
    main()
