# Porcentaje de palabras que terminan con vocal en el texto
# Longitud de la palabra mas larga
# Palabras que tienen vocal en la primera mitad
# Cantidad de verbos en infinitivo (terminación ar, er, ir)


def es_letra(x):
    return x not in (',', ' ', '.')


def es_vocal(x):
    return x in "aeiouAEIOU"


texto = input("Ingrese texto: ")
# texto = "hola mundo azzzz zzzzaz dormir era arra arr alr venir"
while texto[-1] != '.':
    texto = input("Ingrese texto: ")

# Inicialización
clet = cpal = 0
termina_vocal = b_aei = b_verbo = False
palabras_vocal = palabras_vocal_primera = contador_verbos = 0
mas_larga = None
primera_vocal = -1

for car in texto:
    if es_letra(car):
        clet += 1
        # Dentro de palabra
        if es_vocal(car):
            termina_vocal = True
            if primera_vocal == -1:
                primera_vocal = clet
        else:
            termina_vocal = False

        if car in "aeiAEI":
            b_aei = True
            b_verbo = False
        else:
            if car in "rR" and b_aei:
                b_verbo = True
            else:
                b_verbo = False
            b_aei = False

    else:
        if clet > 0:
            cpal += 1
            # Fin de palabra
            if termina_vocal:
                palabras_vocal += 1

            if cpal == 1 or clet > mas_larga:
                mas_larga = clet

            mitad = clet // 2
            if primera_vocal != -1 and primera_vocal <= mitad:
                palabras_vocal_primera += 1

            if b_verbo:
                contador_verbos += 1

        # Reinicialización
        termina_vocal = b_aei = b_verbo = False
        primera_vocal = -1
        clet = 0


# Fin de texto
if cpal > 0:
    porcentaje = palabras_vocal * 100 / cpal
else:
    porcentaje = 0

print(texto)
print("El porcentaje de palabras que terminan en vocal", porcentaje, "%")
print("La longitud de la palabra mas larga es:", mas_larga)
print("Palabras con vocal en la primera mitad:", palabras_vocal_primera)
print("Cantidad de verbos:", contador_verbos)
