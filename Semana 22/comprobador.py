import pila


def comprobar(expresion):
    p = pila.init()

    for letra in expresion:
        if letra in "([{":
            pila.push(p, letra)
        elif letra in ")]}":
            #if pila.empty(p):
                #return False
            if letra == ")" and pila.pop(p) != "(" or \
                    letra == "]" and pila.pop(p) != "[" or \
                    letra == "}" and pila.pop(p) != "{":
                return False

    return pila.empty(p)


if __name__ == '__main__':
    expresiones = [
        "[]",
        "[)",
        "())",
        "()(",
        "{(a + b) [c * d] }"
    ]

    for exp in expresiones:
        res = comprobar(exp)
        print(exp, ":", res)
