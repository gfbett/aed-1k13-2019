import ordenamiento
import random


n = 1000000
v = [0] * n
for i in range(n):
    v[i] = random.randint(1, 1000000)


# v2 = v[:]
#
# print("Ordenando inserción directa: ")
# ordenamiento.insertion_sort(v2)
# print("Fin ordenamiento")

v2 = v[:]

#print(v2)
print("Ordenando quick sort: ")
ordenamiento.quick_sort(v2)
print("Fin ordenamiento")
#print(v2)

