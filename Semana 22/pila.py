def init():
    pila = []
    return pila


def empty(pila):
    return len(pila) == 0


def push(pila, x):
    pila.append(x)

def peek(pila):
    return pila[-1]

def pop(pila):
    res = pila[-1]
    del pila[-1]
    return res


if __name__ == '__main__':
    p = init()
    print(empty(p))
    push(p, 1)
    push(p, 2)
    print(empty(p))
    print(pop(p))
    print(pop(p))
    print(empty(p))
