import vehiculos
import pickle
import os


NOMBRE_ARCHIVO = "autos.bin"

def menu():
    print("1_ Cargar un vehículo")
    print("2_ Vender un vehículo")
    print("3_ Mostrar vehículos")
    print("4_ Filtrar por modelo")
    print("5_ Salir")
    return int(input("Ingrese opción: "))


def agregar_vehiculo(v):
    archivo = open(NOMBRE_ARCHIVO, "ab")
    pickle.dump(v, archivo)
    archivo.close()


def mostrar_vehiculos():
    archivo = open(NOMBRE_ARCHIVO, "rb")
    size = os.path.getsize(NOMBRE_ARCHIVO)
    while archivo.tell() < size:
        v = pickle.load(archivo)
        vehiculos.write(v)

    archivo.close()


def vender_vehiculo(patente):
    archivo = open(NOMBRE_ARCHIVO, "r+b")
    size = os.path.getsize(NOMBRE_ARCHIVO)
    while archivo.tell() < size:
        pos = archivo.tell()
        v = pickle.load(archivo)
        if patente == v.patente:
            v.estado = 0
            archivo.seek(pos)
            pickle.dump(v, archivo)
            break


    archivo.close()


def mostrar_disponibles_modelo(modelo):
    archivo = open(NOMBRE_ARCHIVO, "rb")
    size = os.path.getsize(NOMBRE_ARCHIVO)
    while archivo.tell() < size:
        v = pickle.load(archivo)
        if v.modelo > modelo and v.estado == 1:
            vehiculos.write(v)

    archivo.close()



def principal():
    op = 0
    while op != 5:
        op = menu()
        if op == 1:
            v = vehiculos.cargar_vehiculo()
            agregar_vehiculo(v)
        elif op == 2:
            patente = input("Ingrese patente: ")
            vender_vehiculo(patente)
        elif op == 3:
            mostrar_vehiculos()
        elif op == 4:
            modelo = int(input("Ingrese modelo: "))
            mostrar_disponibles_modelo(modelo)

if __name__ == '__main__':
    principal()
