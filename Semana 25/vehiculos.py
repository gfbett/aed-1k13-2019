ESTADO = ("Vendido", "Disponible")

class Vehiculo:
    def __init__(self, patente, modelo):
        self.patente = patente
        self.estado = 1
        self.modelo = modelo


def write(vehiculo):
    print("Patente:", vehiculo.patente,
          "Estado:", ESTADO[vehiculo.estado],
          "Modelo:", vehiculo.modelo)


def cargar_vehiculo():
    patente = input("Ingrese patente: ")
    modelo = int(input("Ingrese el modelo: "))
    return Vehiculo(patente, modelo)


if __name__ == '__main__':
    v = cargar_vehiculo()
    write(v)
