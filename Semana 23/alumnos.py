import random


def insertar_ordenado(v, nro):
    n = len(v)
    izq = 0
    der = n - 1
    while izq <= der:
        c = (izq + der) // 2
        if v[c] == nro:
            pos = c
            break
        elif v[c] > nro:
            der = c - 1
        else:
            izq = c + 1
    if der < izq:
        pos = izq

    v[pos:pos] = [nro]


def insertar_ordenado_seq(v, nro):
    pos = len(v)
    for i in range(len(v)):
        if v[i] >= nro:
            pos = i
            break
    v[pos:pos] = [nro]



def cargar_vector(n):
    v = []
    for i in range(n):
        nro = random.randint(1, 1000)
        insertar_ordenado(v, nro)

    return v


def principal():
    #n = int(input("Ingrese cantidad: "))
    n = 20000
    v = cargar_vector(n)
    print(v)


if __name__ == '__main__':
    principal()
